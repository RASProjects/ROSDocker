#!/bin/bash

# Allows full network access to docker containers
sudo iptables -I DOCKER-USER -i anywhere -o anywhere -j ACCEPT

# Starts a ROS Indigo container with the host network
docker run -it --network host ros:indigo
