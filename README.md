# ROSDocker

**WARNING:** Under development. Not ready for production.

![GitLab](https://img.shields.io/gitlab/license/RASProjects/ROSDocker?color=darkblue&style=for-the-badge)
![Lines of Code](https://img.shields.io/tokei/lines/gitlab/RASProjects/ROSDocker?style=for-the-badge)
![GitLab issues](https://img.shields.io/gitlab/issues/open/RASProjects/ROSDocker?style=for-the-badge&color=red)
![GitLab closed issues](https://img.shields.io/gitlab/issues/closed-raw/RASProjects/ROSDocker?style=for-the-badge)

Scripts and guides to help using ROS Indigo inside Docker.

## Usage

### Quickstart

To start using ROS Indigo inside docker just run the following commands inside a linux system:

```bash
# Clone the repository
git clone https://gitlab.com/RASProjects/ROSDocker.git
cd ROSDocker

# Start the script
./autostart.sh
```

### Build Custom Images

You can use the `build.sh` script to build all the custom images included in the `docker-compose.yml` file.

```bash
./build.sh
```

### Run a Custom Image

```bash
./run-custom.sh <ros-distro>
# For example: ./run-custom.sh melodic
```

## Support

If you have an issue, please file a report in the issues section of this respository

## Roadmap

Develop a complete tool-set for using ROS within docker.

## Contributing

This repository is completely open for contributions, under the [Contributor Covenant Code Of Conduct](./CONTRIBUTING.md).

## Author

- @JuanCSUCoder - Juan Camilo Sánchez Urrego

## License

This project is licensed under the Massachusetts Institute of Technology (MIT) license. See [LICENSE](./LICENSE) for more information.
