#!/bin/bash

# Allows full network access to docker containers
echo Configuring container networking ...
sudo iptables -I DOCKER-USER -i anywhere -o anywhere -j ACCEPT

echo Trying to start ROS $1 ...
docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix/ --network host rosdocker-$1